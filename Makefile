
all: exec test

exec:
	go build cmd/server/server_main.go
	go build cmd/client/client_main.go

test:
	go test -coverprofile=coverage_client.out ./client/...
	go test -coverprofile=coverage_server.out ./server/...
