A simple file transfer client and server. Transfers files through HTTP.


#BUILDING

```
make exec
```

it will generate `client_main` and `server_main`. Both have handful `-help` command line argument.

#RUNNING

```
./server_main -d "/some/path"
```

```
./client_main -server "http://server.address:3044" -dir "$HOME/Downloads"
```

#TESTING

```
make test
```
