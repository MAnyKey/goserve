package client

import (
	"bytes"
	"io"
	"path"
	"path/filepath"

	"github.com/chzyer/readline"
	"github.com/facebookgo/stackerr"

	"bitbucket.org/MAnyKey/goserve/server"
)

type Client struct {
	downloadDir string
	fileServer  FileServer

	currentDir string
	rl         ReadLine
}

func NewClient(downloadDir string, fileServer FileServer, makeReadLine ReadLineFactory) (*Client, error) {
	completer := readline.NewPrefixCompleter(
		readline.PcItem("cd"),
		readline.PcItem("ls"),
		readline.PcItem("get"),
	)
	rl, err := makeReadLine(&readline.Config{
		Prompt:          makePrompt("/"),
		InterruptPrompt: "^C",
		EOFPrompt:       "exit",
		AutoComplete:    completer,
	})
	if err != nil {
		return nil, stackerr.Wrap(err)
	}
	client := &Client{
		downloadDir: downloadDir,
		fileServer:  fileServer,

		currentDir: "/",
		rl:         rl,
	}
	return client, nil
}

func (client *Client) Run() {
	for {
		line, err := client.rl.Readline()
		if err == readline.ErrInterrupt {
			continue
		} else if err == io.EOF {
			break
		}
		args := splitArgs(line)
		if len(args) == 0 {
			continue
		}

		shouldExit := false
		switch args[0] {
		case "exit":
			shouldExit = true

		case "cd":
			dir := "/"
			if len(args) > 1 {
				dir = args[1]
			}
			client.changeDir(dir)

		case "ls":
			dir := client.currentDir
			if len(args) > 1 {
				dir = args[1]
			}
			client.listDir(dir)

		case "get":
			if len(args) == 1 {
				client.rl.Println("incorrect command, use: get FILE*")
				continue
			}
			for _, name := range args[1:] {
				client.getFile(name)
			}
		}

		if shouldExit {
			break
		}
	}
}

func (client *Client) changeDir(to string) {
	to = client.preparePath(to)
	err := client.fileServer.StatDir(to)
	if err != nil {
		client.rl.Println(err.Error())
		return
	}
	client.currentDir = to
	client.updatePrompt()
}

func (client *Client) listDir(dir string) {
	dir = client.preparePath(dir)
	files, err := client.fileServer.ListDir(dir)
	if err != nil {
		client.rl.Println(err.Error())
		return
	}
	client.printFiles(files)
}

func (client *Client) getFile(file string) {
	file = client.preparePath(file)
	response, err := client.fileServer.GetFile(file)
	if err != nil {
		client.rl.Println(err.Error())
		return
	}
	client.saveFile(response, filepath.Base(file))
}

func (client *Client) preparePath(to string) string {
	if path.IsAbs(to) {
		return path.Clean(to)
	}
	return path.Clean(path.Join(client.currentDir, to))
}

func (client *Client) printFiles(files []server.FileInfo) {
	// TODO: do it the real ls way
	for _, info := range files {
		client.rl.Println(formatFileName(info.Name, info.IsDir))
	}
}

func formatFileName(name string, isDir bool) string {
	if !isDir {
		return name
	}
	return wrapControl(name, []textControl{bold, blue})
}

func (client *Client) updatePrompt() {
	client.rl.SetPrompt(makePrompt(client.currentDir))
}

func makePrompt(dir string) string {
	return wrapControl(dir, []textControl{bold, green}) + "> "
}

func splitArgs(line string) []string {
	vals := []string{}

	var currentString bytes.Buffer
	hadBackslash := false
	for _, char := range line {
		if hadBackslash {
			currentString.WriteRune(char)
			hadBackslash = false
			continue
		}
		switch {
		case char == ' ':
			if currentString.Len() != 0 {
				vals = append(vals, currentString.String())
			}
			currentString.Reset()
		case char == '\\':
			hadBackslash = true
		default:
			currentString.WriteRune(char)
		}
	}
	if currentString.Len() != 0 {
		vals = append(vals, currentString.String())
	}
	return vals
}
