package client

import (
	"encoding/json"
	"errors"
	"io"
	"net/http"
	"net/url"
	"path"
	"strconv"

	"github.com/stretchr/testify/mock"

	"bitbucket.org/MAnyKey/goserve/server"
)

type File struct {
	Body io.ReadCloser
	Size *int64
}

type FileServer interface {
	StatDir(name string) error
	ListDir(name string) ([]server.FileInfo, error)

	GetFile(name string) (File, error)
}

type MockFileServer struct {
	mock.Mock
}

func (m *MockFileServer) StatDir(name string) error {
	result := m.Called(name)
	return result.Error(0)
}

func (m *MockFileServer) ListDir(name string) ([]server.FileInfo, error) {
	result := m.Called(name)
	return result.Get(0).([]server.FileInfo), result.Error(1)
}

func (m *MockFileServer) GetFile(name string) (File, error) {
	result := m.Called(name)
	return result.Get(0).(File), result.Error(1)
}

type HTTPFileServer struct {
	ServerAddress string
}

func (s *HTTPFileServer) preparePath(command, name string) (string, error) {
	if !path.IsAbs(name) {
		return "", errors.New("Path is not absolute")
	}
	parsed, err := url.Parse(s.ServerAddress)
	if err != nil {
		return "", err
	}
	parsed.Path = path.Join(command, name)
	return parsed.String(), nil
}

func (s *HTTPFileServer) StatDir(name string) error {
	addr, err := s.preparePath("list", name)
	if err != nil {
		return err
	}
	resp, err := http.Head(addr)
	if err != nil {
		return err
	}
	defer resp.Body.Close()
	if resp.StatusCode == http.StatusNotFound {
		return errors.New("No such directory")
	}
	if resp.StatusCode != http.StatusOK {
		return errors.New("Something bad happened")
	}
	return nil
}

func (s *HTTPFileServer) ListDir(name string) ([]server.FileInfo, error) {
	addr, err := s.preparePath("list", name)
	if err != nil {
		return nil, err
	}
	resp, err := http.Get(addr)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	if resp.StatusCode == http.StatusNotFound {
		return nil, errors.New("No such directory")
	}
	if resp.StatusCode != http.StatusOK {
		return nil, errors.New("Something bad happened")
	}
	data := []server.FileInfo{}
	decoder := json.NewDecoder(resp.Body)
	err = decoder.Decode(&data)
	if err != nil {
		return nil, err
	}
	return data, nil
}

func (s *HTTPFileServer) GetFile(name string) (file File, err error) {
	addr, err := s.preparePath("get", name)
	if err != nil {
		return
	}
	resp, err := http.Get(addr)
	if err != nil {
		return
	}
	shouldCloseBody := false
	defer func() {
		if shouldCloseBody {
			resp.Body.Close()
		}
	}()
	if resp.StatusCode == http.StatusNotFound {
		shouldCloseBody = true
		err = errors.New("No such file")
		return
	}
	if resp.StatusCode != http.StatusOK {
		shouldCloseBody = true
		err = errors.New("Something bad happened")
		return
	}

	file = File{
		Body: resp.Body,
	}
	contentLength := resp.Header.Get("Content-Length")
	if contentLength != "" {
		value, parseErr := strconv.ParseInt(contentLength, 10, 64)
		if parseErr == nil {
			file.Size = &value
		}
	}
	return
}
