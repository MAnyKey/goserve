package client

import (
	"io"
	"io/ioutil"
	"os"
	"testing"

	"github.com/chzyer/readline"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"bitbucket.org/MAnyKey/goserve/server"
)

func TestSplitArgs(t *testing.T) {
	assert.Equal(t, []string{"get", "foo/bar"}, splitArgs(`get foo/bar`))
	assert.Equal(t, []string{"cd", "dir/do not enter"}, splitArgs(`cd dir/do\ not\ enter`))
	assert.Equal(t, []string{"ls", `slash\in_my_dir`}, splitArgs(`ls slash\\in_my_dir`))
	assert.Equal(t, []string{"ls", "skipsspace"}, splitArgs(`ls    skipsspace`))
	assert.Equal(t, []string{"exit"}, splitArgs("exit"))
}

type testClient struct {
	*Client

	mockFileServer *MockFileServer
	mockReadLine   *MockReadLine
}

func newTestClient(t *testing.T) *testClient {
	mockReadLine := &MockReadLine{}
	mockFileServer := &MockFileServer{}
	makeReadLine := func(*readline.Config) (ReadLine, error) {
		return mockReadLine, nil
	}

	tmp, err := ioutil.TempDir(os.TempDir(), "client_test")
	require.NoError(t, err)

	client, err := NewClient(tmp, mockFileServer, makeReadLine)
	require.NoError(t, err)
	return &testClient{
		Client: client,

		mockFileServer: mockFileServer,
		mockReadLine:   mockReadLine,
	}
}

func TestClientExitOnEOF(t *testing.T) {
	client := newTestClient(t)

	client.mockReadLine.On("Readline").Return("", io.EOF).Once()
	client.Run()
}

func TestClientExit(t *testing.T) {
	client := newTestClient(t)

	client.mockReadLine.On("Readline").Return("exit", nil).Once()
	client.Run()
}

func TestClientListCurDir(t *testing.T) {
	client := newTestClient(t)

	client.mockReadLine.On("Readline").Return("cd foo", nil).Once()
	client.mockReadLine.On("SetPrompt", makePrompt("/foo")).Once()
	client.mockReadLine.On("Readline").Return("ls", nil).Once()
	client.mockReadLine.On("Readline").Return("exit", nil).Once()

	files := []server.FileInfo{
		{
			Name: "test_1",
		},
		{
			Name: "test_2",
		},
	}
	client.mockFileServer.On("StatDir", "/foo").Return(nil).Once()
	client.mockFileServer.On("ListDir", "/foo").Return(files, nil).Once()
	client.Run()

	assert.Equal(t, []string{"test_1", "\n", "test_2", "\n"}, client.mockReadLine.Stdout)
}
