package client

import (
	"bytes"
	"fmt"
	"io"

	"github.com/chzyer/readline"
)

type ReadLine interface {
	Readline() (string, error)
	SetPrompt(ps string)
	Print(str string)
	Println(str string)
}

type ReadLineFactory func(*readline.Config) (ReadLine, error)

func NewReadLine(config *readline.Config) (ReadLine, error) {
	i, err := readline.NewEx(config)
	if err != nil {
		return nil, err
	}
	return &ReadLineImpl{i}, nil
}

type ReadLineImpl struct {
	*readline.Instance
}

func (rl *ReadLineImpl) Print(str string) {
	fmt.Print(str)
}

func (rl *ReadLineImpl) Println(str string) {
	fmt.Println(str)
}

type textControl int

const (
	bold  textControl = 1
	green             = 32
	blue              = 34
	end               = 0
)

func makeControlSequence(code textControl) string {
	return fmt.Sprintf("\033[0%dm", code)
}

func wrapControl(text string, controls []textControl) string {
	var buffer bytes.Buffer
	for _, c := range controls {
		io.WriteString(&buffer, makeControlSequence(c))
	}
	io.WriteString(&buffer, text)
	io.WriteString(&buffer, makeControlSequence(end))
	return buffer.String()
}
