package client

import (
	"github.com/stretchr/testify/mock"
)

type MockReadLine struct {
	mock.Mock

	Stdout []string
}

func (m *MockReadLine) Print(str string) {
	m.Stdout = append(m.Stdout, str)
}

func (m *MockReadLine) Println(str string) {
	m.Stdout = append(m.Stdout, str)
	m.Stdout = append(m.Stdout, "\n")
}

func (m *MockReadLine) Readline() (string, error) {
	result := m.Mock.Called()
	return result.Get(0).(string), result.Error(1)
}

func (m *MockReadLine) SetPrompt(newPrompt string) {
	m.Mock.Called(newPrompt)
}
