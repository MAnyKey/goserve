package client

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestWrapControl(t *testing.T) {
	actual := wrapControl("/foo/bar", []textControl{bold, green})
	expected := "\033[01m\033[032m/foo/bar\033[00m"
	assert.Equal(t, expected, actual)
}
