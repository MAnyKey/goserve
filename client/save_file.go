package client

import (
	"errors"
	"fmt"
	"io"
	"os"
	"path"
)

func (client *Client) saveFile(file File, filename string) {
	output, err := os.Create(path.Join(client.downloadDir, filename))
	if err != nil {
		client.rl.Println(err.Error())
		return
	}

	progressFunction := func(progress float64) {
		text := fmt.Sprintf("\rSaving file %s progress: %d%%", filename, int(progress*100))
		client.rl.Print(text)
	}

	err = copyProgress(file, output, progressFunction)
	if err != nil {
		client.rl.Println(err.Error())
		return
	}
	client.rl.Println("")
	client.rl.Println("Done")
}

type progressFunc func(float64)

var ErrContentLengthMismatch = errors.New("Content length mismatch")

// this is a copy of io.Copy that can track progress
func copyProgress(file File, output io.Writer, progress progressFunc) (err error) {
	if file.Size == nil {
		_, err = io.Copy(output, file.Body)
		return err
	}

	var written int64 = 0
	buf := make([]byte, 32*1024)
	for {
		progress(float64(written) / float64(*file.Size))
		nr, er := file.Body.Read(buf)
		if nr > 0 {
			nw, ew := output.Write(buf[0:nr])
			if nw > 0 {
				written += int64(nw)
			}
			if ew != nil {
				err = ew
				break
			}
			if nr != nw {
				err = io.ErrShortWrite
				break
			}
		}
		if er == io.EOF {
			break
		}
		if er != nil {
			err = er
			break
		}
	}
	if written != *file.Size {
		err = ErrContentLengthMismatch
	}
	return
}
