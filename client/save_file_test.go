package client

import (
	"bytes"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"math/rand"
	"path"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
)

type savedReader struct {
	data [][]byte

	curIdx int
}

func (r *savedReader) Read(buf []byte) (int, error) {
	if r.curIdx == len(r.data) {
		return 0, io.EOF
	}
	if len(buf) < len(r.data[r.curIdx]) {
		return 0, errors.New("Too small")
	}
	written := copy(buf, r.data[r.curIdx])
	r.curIdx++
	return written, nil
}

type writeProgress struct {
	mock.Mock
}

func (p *writeProgress) progress(progress float64) {
	p.Mock.Called(progress)
}

func TestCopyProgress(t *testing.T) {
	reader := &savedReader{
		data: [][]byte{
			[]byte("abc"),
			[]byte("cd"),
		},
	}

	size := int64(0)
	for _, d := range reader.data {
		size += int64(len(d))
	}

	file := File{
		Body: ioutil.NopCloser(reader),
		Size: &size,
	}

	var buffer bytes.Buffer
	mprogress := &writeProgress{}

	mprogress.On("progress", 0.0).Once()
	mprogress.On("progress", float64(3)/float64(5)).Once()
	mprogress.On("progress", 1.0).Once()
	err := copyProgress(file, &buffer, mprogress.progress)
	require.NoError(t, err)

	assert.Equal(t, []byte("abccd"), buffer.Bytes())
}

func TestClientGetFile(t *testing.T) {
	client := newTestClient(t)

	client.mockReadLine.On("Readline").Return("get foo/bar", nil).Once()
	client.mockReadLine.On("Readline").Return("exit", nil).Once()

	r := rand.New(rand.NewSource(99))
	chunks := [][]byte{}
	data := []byte{}
	for i := 0; i < 10; i++ {
		datum := make([]byte, 10)
		r.Read(datum)
		chunks = append(chunks, datum)
		data = append(data, datum...)
	}
	reader := &savedReader{
		data: chunks,
	}
	dataLength := int64(len(data))
	file := File{
		Body: ioutil.NopCloser(reader),
		Size: &dataLength,
	}

	client.mockFileServer.On("GetFile", "/foo/bar").Return(file, nil).Once()

	client.Run()

	expectedStdout := []string{}
	for i := 0; i <= 10; i++ {
		expectedStdout = append(expectedStdout, fmt.Sprintf("\rSaving file bar progress: %d%%", i*10))
	}
	expectedStdout = append(expectedStdout, "", "\n", "Done", "\n")
	assert.Equal(t, expectedStdout, client.mockReadLine.Stdout)

	actualData, err := ioutil.ReadFile(path.Join(client.downloadDir, "bar"))
	require.NoError(t, err)
	assert.Equal(t, data, actualData)
}
