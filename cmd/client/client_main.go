package main

import (
	"os"
	"fmt"
	"flag"
	"log"

	"bitbucket.org/MAnyKey/goserve/client"
)

func main() {
	flagSet := flag.NewFlagSet("client", flag.ExitOnError)
	serverPtr := flagSet.String("server", "http://localhost:3044", "data server address")
	dirPtr := flagSet.String("dir", ".", "directory to download to")
	flagSet.Parse(os.Args[1:])

	if *serverPtr == "" {
		fmt.Println("You should provide server address")
		flagSet.PrintDefaults()
		return
	}
	fileServer := &client.HTTPFileServer{
		ServerAddress: *serverPtr,
	}

	client, err := client.NewClient(*dirPtr, fileServer, client.NewReadLine)
	if err != nil {
		log.Fatal(err)
	}
	client.Run()
}
