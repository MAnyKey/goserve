package main

import (
	"flag"
	"log"
	"os"

	"bitbucket.org/MAnyKey/goserve/server"
)

func main() {
	flagSet := flag.NewFlagSet("server", flag.ExitOnError)
	portPtr := flagSet.String("p", "3044", "port to listening")
	dirPtr := flagSet.String("d", ".", "directory to serve")
	flagSet.Parse(os.Args[1:])

	fs_driver, err := server.NewFSDriver(*dirPtr)
	if err != nil {
		log.Fatal(err)
	}
	err = server.ListenAndServe(fs_driver, *portPtr)
	if err != nil {
		log.Fatal(err)
	}
}
