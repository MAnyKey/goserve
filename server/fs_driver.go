package server

import (
	"io"
	"os"
	"path"
	"path/filepath"

	"github.com/facebookgo/stackerr"
)

type FSDriver interface {
	List(path string) ([]os.FileInfo, error)
	Get(path string) (io.ReadCloser, os.FileInfo, error)
}

type FSErrorType int

const (
	NotFound FSErrorType = iota
	BadType
	TransientError
)

type FSError struct {
	ErrorType FSErrorType
	Err       error
}

func (Err *FSError) Error() string {
	return Err.Err.Error()
}

func NewFSError(errorType FSErrorType, description string) *FSError {
	return &FSError{
		ErrorType: errorType,
		Err:       stackerr.New(description),
	}
}

func WrapFSError(errorType FSErrorType, err error) *FSError {
	return &FSError{
		ErrorType: errorType,
		Err:       stackerr.Wrap(err),
	}
}

func IsNotFoundError(err error) bool {
	if fsError, ok := err.(*FSError); ok {
		return fsError.ErrorType == NotFound
	}
	return false
}

func IsBadTypeError(err error) bool {
	if fsError, ok := err.(*FSError); ok {
		return fsError.ErrorType == BadType
	}
	return false
}

type FSDriverImpl struct {
	root string
}

func (driver *FSDriverImpl) Get(path string) (io.ReadCloser, os.FileInfo, error) {
	file, err := os.Open(driver.preparePath(path))
	if err != nil {
		// Technically, here could be insufficient permissions error (and the
		// bunch of other ones), but here we cannot safely distinguish between these.
		return nil, nil, WrapFSError(NotFound, err)
	}
	shouldCloseFile := false
	defer func() {
		if shouldCloseFile {
			file.Close()
		}
	}()
	fileInfo, err := file.Stat()
	if err != nil {
		shouldCloseFile = true
		return nil, nil, WrapFSError(TransientError, err)
	}
	if fileInfo.IsDir() {
		shouldCloseFile = true
		return nil, nil, NewFSError(BadType, "Is not a regular file")
	}
	return file, fileInfo, nil
}

func (driver *FSDriverImpl) List(path string) ([]os.FileInfo, error) {
	file, err := os.Open(driver.preparePath(path))
	if err != nil {
		return nil, WrapFSError(NotFound, err)
	}
	defer file.Close()

	files, err := file.Readdir(-1)
	if err != nil {
		// Could be EACCESS error, see same comment in Get() above
		return nil, WrapFSError(BadType, err)
	}
	return files, nil
}

func NewFSDriver(root string) (*FSDriverImpl, error) {
	file, err := os.Open(root)
	if err != nil {
		return nil, WrapFSError(NotFound, err)
	}
	defer file.Close()
	fileInfo, err := file.Stat()
	if err != nil {
		return nil, WrapFSError(NotFound, err)
	}
	if !fileInfo.IsDir() {
		return nil, NewFSError(BadType, "Root is not a directory")
	}
	return &FSDriverImpl{root: root}, nil
}

func (driver *FSDriverImpl) preparePath(name string) string {
	return filepath.Join(driver.root, filepath.FromSlash(path.Clean("/"+name)))
}
