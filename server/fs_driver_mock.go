package server

import (
	"io"
	"os"

	"github.com/stretchr/testify/mock"
)

type FSDriverMock struct {
	mock.Mock
}

func (mock *FSDriverMock) Get(path string) (io.ReadCloser, os.FileInfo, error) {
	result := mock.Mock.Called(path)
	reader := result.Get(0)
	if reader != nil {
		return reader.(io.ReadCloser), result.Get(1).(os.FileInfo), result.Error(2)
	}
	return nil, nil, result.Error(2)
}

func (mock *FSDriverMock) List(path string) ([]os.FileInfo, error) {
	result := mock.Mock.Called(path)
	files := result.Get(0)
	if files != nil {
		return files.([]os.FileInfo), result.Error(1)
	}
	return nil, result.Error(1)
}

func NewFSDriverMock() *FSDriverMock {
	return &FSDriverMock{}
}
