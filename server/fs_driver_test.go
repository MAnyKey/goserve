package server

import (
	"io/ioutil"
	"os"
	"path/filepath"
	"sort"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestDontServeHigherThanRoot(t *testing.T) {
	tmp := os.TempDir()
	driver, err := NewFSDriver(tmp)
	require.NoError(t, err)
	prepared := driver.preparePath("../home")
	assert.Equal(t, filepath.Join(tmp, "/home"), prepared)
	prepared1 := driver.preparePath("../home/../../../../")
	assert.Equal(t, filepath.Join(tmp, "/"), prepared1)
}

var testFileMode = os.FileMode(0644)
var testDirMode = os.ModeDir | os.FileMode(0755)

type Dir map[string]interface{}

func createDir(t *testing.T, dirPath string, dir Dir) {
	for name, contents := range dir {
		switch contents := contents.(type) {
		case []byte:
			err := ioutil.WriteFile(filepath.Join(dirPath, name), contents, testFileMode)
			require.NoError(t, err)
		case Dir:
			subdirPath := filepath.Join(dirPath, name)
			err := os.MkdirAll(subdirPath, testDirMode)
			require.NoError(t, err)
			createDir(t, subdirPath, contents)
		}
	}
}

func checkDirectoryList(t *testing.T, driver FSDriver, name string, dir Dir) {
	expectedContents := []string{}
	for name, _ := range dir {
		expectedContents = append(expectedContents, name)
	}
	sort.Strings(expectedContents)

	result, err := driver.List(name)
	require.NoError(t, err)

	actualNames := []string{}
	for _, fileInfo := range result {
		actualNames = append(actualNames, fileInfo.Name())
	}
	sort.Strings(actualNames)

	assert.Equal(t, expectedContents, actualNames)
}

var sampleDir = Dir{
	"dir1": Dir{
		"subdir": Dir{
			"file1": []byte("data1"),
			"file2": []byte("data2"),
		},
	},
	"dir2": Dir{
		"file3": []byte("data3"),
	},
	"abc": []byte(""),
	"cba": []byte("abc"),
}

func TestSampleDir(t *testing.T) {
	root, err := ioutil.TempDir(os.TempDir(), "fs_driver_test")
	require.NoError(t, err)
	createDir(t, root, sampleDir)

	defer func() {
		err = os.RemoveAll(root)
		require.NoError(t, err)
	}()

	driver, err := NewFSDriver(root)
	require.NoError(t, err)

	checkDirectoryList(t, driver, "/", sampleDir)

	contents, _, err := driver.Get("/dir1/subdir/file1")
	require.NoError(t, err)
	defer contents.Close()
	data, err := ioutil.ReadAll(contents)
	require.NoError(t, err)

	assert.Equal(t, []byte("data1"), data)
}

func TestDontServeFile(t *testing.T) {
	root, err := ioutil.TempDir(os.TempDir(), "fs_driver_test")
	require.NoError(t, err)
	testDir := Dir{
		"file": []byte(""),
	}
	createDir(t, root, testDir)

	defer func() {
		err = os.RemoveAll(root)
		require.NoError(t, err)
	}()

	_, err = NewFSDriver(filepath.Join(root, "file"))
	require.Error(t, err)
}

func TestDontServeInexistentDir(t *testing.T) {
	root := filepath.Join(os.TempDir(), "fs_driver_test_IAMNOTHERE")
	_, err := NewFSDriver(root)
	require.Error(t, err)
}

func TestDontListInexistentDir(t *testing.T) {
	root, err := ioutil.TempDir(os.TempDir(), "fs_driver_test")
	require.NoError(t, err)

	defer func() {
		err = os.RemoveAll(root)
		require.NoError(t, err)
	}()

	driver, err := NewFSDriver(root)
	require.NoError(t, err)

	_, err = driver.List("/dir")
	require.Error(t, err)
}

func TestDontGetInexistentFile(t *testing.T) {
	root, err := ioutil.TempDir(os.TempDir(), "fs_driver_test")
	require.NoError(t, err)

	defer func() {
		err = os.RemoveAll(root)
		require.NoError(t, err)
	}()

	driver, err := NewFSDriver(root)
	require.NoError(t, err)

	_, _, err = driver.Get("/file")
	require.Error(t, err)
}

func TestDontListFile(t *testing.T) {
	root, err := ioutil.TempDir(os.TempDir(), "fs_driver_test")
	require.NoError(t, err)
	testDir := Dir{
		"file": []byte(""),
	}
	createDir(t, root, testDir)

	defer func() {
		err = os.RemoveAll(root)
		require.NoError(t, err)
	}()

	driver, err := NewFSDriver(root)
	require.NoError(t, err)

	_, err = driver.List("/file")
	require.Error(t, err)
}

func TestDontGetDirectory(t *testing.T) {
	root, err := ioutil.TempDir(os.TempDir(), "fs_driver_test")
	require.NoError(t, err)
	testDir := Dir{
		"dir": Dir{},
	}
	createDir(t, root, testDir)

	defer func() {
		err = os.RemoveAll(root)
		require.NoError(t, err)
	}()

	driver, err := NewFSDriver(root)
	require.NoError(t, err)

	_, _, err = driver.Get("/dir")
	require.Error(t, err)
}
