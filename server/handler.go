package server

import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"strings"
)

type Handler struct {
	Driver       FSDriver
	VerboseError bool
}

func NewHandler(driver FSDriver) *Handler {
	return &Handler{
		Driver:       driver,
		VerboseError: false,
	}
}

type FileInfo struct {
	Name  string `json:"name"`
	IsDir bool   `json:"is_dir"`
	Size  int64  `json:"size"`
}

func (handler *Handler) ListHandler(w http.ResponseWriter, r *http.Request) {
	log.Println(r)
	requestPath := strings.TrimPrefix(r.URL.Path, "/list")
	list, err := handler.Driver.List(requestPath)
	if err != nil {
		handler.writeError(w, err)
		return
	}

	files := []FileInfo{}
	for _, info := range list {
		files = append(files, FileInfo{
			Name:  info.Name(),
			IsDir: info.IsDir(),
			Size:  info.Size(),
		})
	}
	encoder := json.NewEncoder(w)
	// we swallow error here, don't we?
	err = encoder.Encode(files)
}

func (handler *Handler) GetHandler(w http.ResponseWriter, r *http.Request) {
	log.Println(r)
	requestPath := strings.TrimPrefix(r.URL.Path, "/get")
	file, fileInfo, err := handler.Driver.Get(requestPath)
	if err != nil {
		handler.writeError(w, err)
		return
	}
	defer file.Close()

	w.Header().Set("Content-Length", fmt.Sprintf("%v", fileInfo.Size()))
	// we swallow error here, don't we?
	_, err = io.Copy(w, file)
}

func ListenAndServe(driver FSDriver, port string) error {
	h := NewHandler(driver)
	serveMux := http.NewServeMux()
	serveMux.HandleFunc("/list/", h.ListHandler)
	serveMux.HandleFunc("/get/", h.GetHandler)
	return http.ListenAndServe(":"+port, serveMux)
}

func errToHttpCode(err error) int {
	if IsNotFoundError(err) {
		return http.StatusNotFound
	}
	if IsBadTypeError(err) {
		return http.StatusBadRequest
	}
	return http.StatusInternalServerError
}

func (handler *Handler) writeError(w http.ResponseWriter, err error) {
	w.WriteHeader(errToHttpCode(err))
	if handler.VerboseError {
		log.Println(err.Error())
		io.WriteString(w, err.Error())
	}
}
