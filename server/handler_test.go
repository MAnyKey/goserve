package server

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"
	"time"

	"github.com/facebookgo/stackerr"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

type MockFileInfo struct {
	Name_    string      `json:"name"`
	Size_    int64       `json:"size"`
	Mode_    os.FileMode `json:"mode"`
	ModTime_ time.Time   `json:"modtime"`
	IsDir_   bool        `json:"isdir"`
}

func (mi MockFileInfo) Name() string {
	return mi.Name_
}

func (mi MockFileInfo) Size() int64 {
	return mi.Size_
}

func (mi MockFileInfo) Mode() os.FileMode {
	return mi.Mode_
}

func (mi MockFileInfo) ModTime() time.Time {
	return mi.ModTime_
}

func (mi MockFileInfo) IsDir() bool {
	return mi.IsDir_
}

func (mi MockFileInfo) Sys() interface{} {
	return nil
}

func TestSmokeGetHandler(t *testing.T) {
	mockDriver := NewFSDriverMock()
	handler := NewHandler(mockDriver)

	responseRecorder := httptest.NewRecorder()
	request, err := http.NewRequest("GET", "http://locahost/get/file", nil)
	require.NoError(t, err)

	data := []byte("test file data")
	mockDriver.On("Get", "/file").
		Return(ioutil.NopCloser(bytes.NewReader(data)), MockFileInfo{Name_:"file", Size_: int64(len(data))}, nil).
		Once()

	handler.GetHandler(responseRecorder, request)

	assert.Equal(t, http.StatusOK, responseRecorder.Code)
	assert.Equal(t, data, responseRecorder.Body.Bytes())
}

func TestSmokeListHandler(t *testing.T) {
	mockDriver := NewFSDriverMock()
	handler := NewHandler(mockDriver)

	responseRecorder := httptest.NewRecorder()
	request, err := http.NewRequest("GET", "http://locahost/list/", nil)
	require.NoError(t, err)

	files := []os.FileInfo{
		MockFileInfo{Name_: "a"},
		MockFileInfo{Name_: "b"},
		MockFileInfo{Name_: "c"},
	}
	mockDriver.On("List", "/").
		Return(files, nil).
		Once()

	handler.ListHandler(responseRecorder, request)

	assert.Equal(t, http.StatusOK, responseRecorder.Code)
	parsedFiles := []MockFileInfo{}
	err = json.Unmarshal(responseRecorder.Body.Bytes(), &parsedFiles)
	require.NoError(t, err)

	actualFiles := []os.FileInfo{}
	for _, value := range parsedFiles {
		actualFiles = append(actualFiles, value)
	}
	assert.Equal(t, files, actualFiles)
}

func TestNotFoundFile(t *testing.T) {
	mockDriver := NewFSDriverMock()
	handler := NewHandler(mockDriver)

	responseRecorder := httptest.NewRecorder()
	request, err := http.NewRequest("GET", "http://locahost/get/file", nil)
	require.NoError(t, err)

	mockDriver.On("Get", "/file").
		Return(nil, nil, NewFSError(NotFound, "Not found")).
		Once()

	handler.GetHandler(responseRecorder, request)

	assert.Equal(t, http.StatusNotFound, responseRecorder.Code)
}

func TestNotFoundDir(t *testing.T) {
	mockDriver := NewFSDriverMock()
	handler := NewHandler(mockDriver)

	responseRecorder := httptest.NewRecorder()
	request, err := http.NewRequest("GET", "http://locahost/list/dir", nil)
	require.NoError(t, err)

	mockDriver.On("List", "/dir").
		Return(nil, NewFSError(NotFound, "Not found")).
		Once()

	handler.ListHandler(responseRecorder, request)

	assert.Equal(t, http.StatusNotFound, responseRecorder.Code)
}

func TestListFile(t *testing.T) {
	mockDriver := NewFSDriverMock()
	handler := NewHandler(mockDriver)

	responseRecorder := httptest.NewRecorder()
	request, err := http.NewRequest("GET", "http://locahost/list/file", nil)
	require.NoError(t, err)

	mockDriver.On("List", "/file").
		Return(nil, NewFSError(BadType, "Trying to list a file")).
		Once()

	handler.ListHandler(responseRecorder, request)

	assert.Equal(t, http.StatusBadRequest, responseRecorder.Code)
}

func TestSomethingBadHappened(t *testing.T) {
	mockDriver := NewFSDriverMock()
	handler := NewHandler(mockDriver)

	responseRecorder := httptest.NewRecorder()
	request, err := http.NewRequest("GET", "http://locahost/list/", nil)
	require.NoError(t, err)

	mockDriver.On("List", "/").
		Return(nil, stackerr.New("BOOM")).
		Once()

	handler.ListHandler(responseRecorder, request)

	assert.Equal(t, http.StatusInternalServerError, responseRecorder.Code)
}
